package com.example.uploader;

import android.util.Log;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uploader.adapters.FilesAdapter;
import com.example.uploader.adapters.UploadingFileAdapter;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static final String GLOBAL_TAG = "GLOBAL_TAG";


    public static byte[] properByteArray(long fileSize) {

        byte[] byteArr;


        // set proper length for byte array based on file size to read file proper


        if (fileSize <= 1048576) {
            //100KB
            byteArr = new byte[102400];
        } else if (fileSize <= 104857600) {
            //1Mb
            byteArr = new byte[1048576];
        } else {
            //10Mb
            byteArr = new byte[10485760];
        }

        return byteArr;
    }


    public static String getFileSize(float fileSize) {


        if(fileSize>=Math.pow(2,20)){

            if(fileSize>=Math.pow(2,30)){
                String res = String.format("%.2fGB",fileSize/Math.pow(10,9));

                return res;
            }

            //convert file size to MB

            String res = String.format("%.2fMB",fileSize/Math.pow(10,6));

            return res;

        }else{
            String res = String.format("%.2fKB",fileSize/Math.pow(10,3));

            return res;
        }
    }


    @BindingAdapter("setUploadingAdapter")
    public static void setUploadingAdapter(RecyclerView view, UploadingFileAdapter adapter){
        if(adapter!=null){
            Log.i(GLOBAL_TAG, "setAdapter: ");
            view.setAdapter(adapter);

        }
    }



}
