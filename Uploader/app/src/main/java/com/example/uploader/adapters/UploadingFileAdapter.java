package com.example.uploader.adapters;

import static com.example.uploader.Utils.GLOBAL_TAG;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uploader.databinding.ModelUploadingFileBinding;
import com.example.uploader.models.UploadFileMode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UploadingFileAdapter extends RecyclerView.Adapter<UploadingFileAdapter.ViewHolder> {

    List<UploadFileMode> uploads = new ArrayList<>();
    private static final String TAG = "UploadsAdapter";

    @NonNull
    @Override
    public UploadingFileAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ModelUploadingFileBinding binding = ModelUploadingFileBinding.inflate(inflater, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull UploadingFileAdapter.ViewHolder holder, int position) {
        onBindViewHolder(holder, position, Collections.emptyList());
    }

    @Override
    public void onBindViewHolder(@NonNull UploadingFileAdapter.ViewHolder holder, int position, @NonNull List payloads) {

        if (payloads.isEmpty()) {


            bindAddedItem(holder, position);

        } else {
            bindUpdatedItem(holder, position, payloads);
        }

    }

    private void bindUpdatedItem(ViewHolder holder, int position, List payloads) {

        int percent = (int) payloads.get(0);

        holder.binding.txUploadPercent.setText(String.valueOf(percent + "%"));

        if (percent > 0) {
            if (percent == 100) {

                holder.binding.pbUploadPercent.setVisibility(View.GONE);
                holder.binding.txUploadPercent.setVisibility(View.GONE);
                holder.binding.ibDownloadedCorrect.setVisibility(View.VISIBLE);
                holder.binding.ibFileUploading.setVisibility(View.GONE);

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.binding.pbUploadPercent.setIndeterminate(false);
                    holder.binding.pbUploadPercent.setProgress(percent, true);
                } else {
                    holder.binding.pbUploadPercent.setProgress(percent);
                }
            }

        }else if(percent==-1){

            holder.binding.pbUploadPercent.setVisibility(View.GONE);
            holder.binding.txUploadPercent.setVisibility(View.GONE);
            holder.binding.ibDownloadedError.setVisibility(View.VISIBLE);
            holder.binding.ibFileUploading.setVisibility(View.GONE);
        }

    }

    private void bindAddedItem(UploadingFileAdapter.ViewHolder holder, int position) {


        holder.binding.txFileUploadedTime.setText(uploads.get(position).getFileAddedTime());
        holder.binding.txUploadFileName.setText(uploads.get(position).getFileName());
        holder.binding.txFileUploadedVolume.setText(uploads.get(position).getFileSize());

        if(uploads.get(position).getPercent()==100){

            holder.binding.pbUploadPercent.setVisibility(View.GONE);
            holder.binding.txUploadPercent.setVisibility(View.GONE);
            holder.binding.ibDownloadedCorrect.setVisibility(View.VISIBLE);
            holder.binding.ibFileUploading.setVisibility(View.GONE);

        }




    }

    @Override
    public int getItemCount() {
        return uploads.size();
    }

    public void addNewUpload(UploadFileMode model) {

        uploads.add(model);



        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                notifyItemInserted(uploads.size() - 1);


            }
        });


    }

    public void updateUploads(int percent, String fileName) {



        for (int i = 0; i < uploads.size(); i++) {

            if (uploads.get(i).getFileName().equals(fileName)) {


                uploads.get(i).setPercent(percent);

                int finali = i;

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {

                        notifyItemChanged(finali, percent);

                    }
                });
                break;
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ModelUploadingFileBinding binding = null;

        public ViewHolder(@NonNull ModelUploadingFileBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }
}
