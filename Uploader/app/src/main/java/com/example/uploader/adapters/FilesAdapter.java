package com.example.uploader.adapters;


import static com.example.uploader.Utils.GLOBAL_TAG;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.uploader.models.FileInfoModel;
import com.example.uploader.databinding.ModelFileBinding;
import com.example.uploader.databinding.ModelLoadingBinding;

import java.util.ArrayList;
import java.util.List;

public class FilesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



    public List<FileInfoModel> filesInfoList = new ArrayList<>();


    @Override
    public int getItemViewType(int position) {
        return filesInfoList.get(position)==null?1:2;
    }

    @Override
    public int getItemCount() {
        return filesInfoList.size();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if(viewType==1){
            ModelLoadingBinding binding = ModelLoadingBinding.inflate(inflater,parent,false);
            return new LoadingViewHolder(binding);
        }else{
            ModelFileBinding binding = ModelFileBinding.inflate(inflater,parent,false);
            return new ViewHolder(binding);
        }




    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        if(holder instanceof ViewHolder){
            ((ViewHolder)holder).bindData(filesInfoList.get(position));
        }



    }


    public void updateList(List<FileInfoModel> mList){
        for (int i = 0; i < mList.size(); i++) {

            filesInfoList.add(mList.get(i));

        }
        if(mList.size()>=10){

            filesInfoList.add(null);

        }
        notifyItemInserted(filesInfoList.size()-1);
    }





    class ViewHolder extends RecyclerView.ViewHolder{

        public ModelFileBinding binding;




        public ViewHolder(@NonNull ModelFileBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindData(FileInfoModel model){
            binding.setModel(model);
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder{

        public ModelLoadingBinding binding;




        public LoadingViewHolder(@NonNull ModelLoadingBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }


    }


}
