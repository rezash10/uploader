package com.example.uploader.models;

import java.util.List;

public class JsonFileModel {
    private List<String> names;
    private List<String> sizes;
    private List<String> times;

    public JsonFileModel(List<String> names, List<String> sizes, List<String> times) {
        this.names = names;
        this.sizes = sizes;
        this.times = times;
    }

    public List<String> getNames() {
        return names;
    }

    public List<String> getSizes() {
        return sizes;
    }

    public List<String> getTimes() {
        return times;
    }


    public void addNames(List<String> newNames){
        if(names!=null){
            names.addAll(newNames);
        }
    }

    public void addSizes(List<String> newSizes){
        if(names!=null){
            names.addAll(newSizes);
        }
    }


    public void addTimes(List<String> newTimes){
        if(names!=null){
            names.addAll(newTimes);
        }
    }
}
