package com.example.uploader;


import com.example.uploader.models.FileInfoModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public class UploadService {

   static UploadService instance;
   static public UploadApis apis;
     public  static String ip;
    ;

    static public UploadService getInstance(String strIP){

        if(instance==null|| !strIP.equals(ip)){
            ip = strIP;
            String baseUrl= "http://"+ip+"/Uploader/";
            instance = new UploadService(baseUrl);
        }

        return instance;
    }

    private UploadService(String url){
        implementRetrofit(url);
    }

    private void implementRetrofit(String url) {


        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(url)
                .build();
        apis = retrofit.create(UploadApis.class);
    }

    public interface UploadApis{
        //-------------post media files to server-------------//
        @Multipart
        @POST("UploadMedia.php")
         Call<String> uploadMedia(@Part MultipartBody.Part parts,@Part MultipartBody.Part fileSize);


        //-------------post documents files to server-------------//

        @FormUrlEncoded
        @POST("UploadDocument.php")
        Call<String> uploadDoc(@Field("file_name") String fileName,@Field("base_64") String encoededBase64
        ,@Field("File_size")String fileSize);

        //-------------get time from server-------------//

        @GET("GetTime.php")
        Call<String> getTime();


        //-------------get files info from server --------------//

        @Headers("Accept: application/json")
        @GET("GetFiles.php")
        Call<List<FileInfoModel>> getFiles(@Query("offset") int offset, @Query("limit") int limit);


    }



}
