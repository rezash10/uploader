package com.example.uploader;

import static com.example.uploader.Utils.GLOBAL_TAG;
import static com.example.uploader.Utils.properByteArray;

import android.util.Log;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

public class ProgressRequestBody extends RequestBody {

    private String type;
    private String fileName;
    private File file;
    private UpdateProgress updateProgress;
    private static final String TAG = "ProgressClass";

    public ProgressRequestBody(String fileName,String type, File file,UpdateProgress progress) {
        this.type = type;
        this.file = file;
        updateProgress = progress;
        this.fileName = fileName;
    }


    @Nullable
    @Override
    public MediaType contentType() {
        return MediaType.parse(type);
    }

    @Override
    public void writeTo(BufferedSink sink) throws IOException {

        FileInputStream inputStream = new FileInputStream(file);

        float uploaded = 0;
        long fileSize = file.length();

        byte[] bytesArr = properByteArray(fileSize);




        try{
            while(true){

                int read = inputStream.read(bytesArr);


                if(read==-1){
                    break;
                }


                uploaded+=read;

                sink.write(bytesArr,0,read);

                int percent = (int) ((uploaded/fileSize)*100);

                updateProgress.setProgress(percent,fileName);

            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
