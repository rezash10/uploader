package com.example.uploader.models;

public class FileInfoModel {

    private String name;
    private String size;
    private String time;

    public FileInfoModel(String name, String size, String time) {
        this.name = name;
        this.size = size;
        this.time = time;
    }


    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }

    public String getTime() {
        return time;
    }
}
