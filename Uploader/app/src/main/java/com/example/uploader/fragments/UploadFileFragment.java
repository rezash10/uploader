package com.example.uploader.fragments;



import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewGroupCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.uploader.R;
import com.example.uploader.UploadService;
import com.example.uploader.databinding.DialogSetFileNameBinding;
import com.example.uploader.databinding.FragmentUploadFileBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.transition.MaterialSharedAxis;


public class UploadFileFragment extends Fragment {

    //-------------Variables-------------//
    ActivityResultLauncher<Intent> openDocumentResult;
    String TAG = getClass().getName();
    UploadService uploadService;
    UploadFileViewModel uploadViewModel;
    Fragment current;
    FragmentUploadFileBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        //----------register for activity result----------//
        openDocumentResult = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult()
                , new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {

                        if (result.getResultCode() == Activity.RESULT_OK) {

                            Uri fileUri = result.getData().getData();


                            showDialog(fileUri);

                        }
                    }
                }
        );
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentUploadFileBinding.inflate(inflater, container, false);

        uploadViewModel = new ViewModelProvider(this).get(UploadFileViewModel.class);

        binding.setViewModel(uploadViewModel);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            ViewGroupCompat.setTransitionGroup(binding.uploadsLayout, true);
        }


        current = requireActivity().getSupportFragmentManager().findFragmentById(R.id.navHostFragment)
                .getChildFragmentManager().getFragments().get(0);


        binding.btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (hasConnection()) {
                    //--------go to fileSystem using saf---------//

                    String ipAddress = binding.etIp.getText().toString().trim();


                    if (!ipAddress.isEmpty()) {


                        uploadService = UploadService.getInstance(ipAddress);


                        Intent fileSystemIntent;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            fileSystemIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                        } else {
                            fileSystemIntent = new Intent(Intent.ACTION_PICK);
                        }
                        fileSystemIntent.setType("*/*");

                        openDocumentResult.launch(fileSystemIntent);
                    } else {
                        Snackbar.make(binding.getRoot(), "لطف ip خود را واردکنید",
                                Snackbar.LENGTH_LONG).show();
                    }


                } else {
                    Snackbar.make(binding.getRoot(), "لطفا به اینترنت متصل شوید",
                            Snackbar.LENGTH_LONG).show();
                }


            }
        });


        uploadViewModel.hasUploads.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    binding.ivNoUploads.setVisibility(View.GONE);
                    binding.txNoUploads.setVisibility(View.GONE);
                    binding.rvUploadingFiles.setVisibility(View.VISIBLE);
                }
            }
        });


        setHasOptionsMenu(true);

        return binding.getRoot();

    }

    private boolean hasConnection() {

        ConnectivityManager manager = (ConnectivityManager) requireContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                || manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        return false;
    }


    private void showDialog(Uri path) {

        //----------show a dialog -----------//

        DialogSetFileNameBinding dialogBinding = DialogSetFileNameBinding.inflate(getLayoutInflater());

        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireActivity(), R.style.DialogStyle)
                .setTitle("نام فایل")
                .setView(dialogBinding.getRoot())
                .setPositiveButton("آپلود", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (hasConnection()) {


                            String name = dialogBinding.etFileName.getEditText().getText().toString();

                            uploadViewModel.uploadFile(path, name);


                        } else {
                            Snackbar.make(binding.getRoot(), "لطفا به اینترنت متصل شوید",
                                    Snackbar.LENGTH_LONG).show();
                        }


                    }
                })
                .setNegativeButton("لغوکردن", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setCancelable(true);

        builder.show();
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {

        inflater.inflate(R.menu.menu_main, menu);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.item_uploaded_file) {

            if(!binding.etIp.getText().toString().isEmpty()){
                UploadService.ip = binding.etIp.getText().toString().trim();
            }

            current.setExitTransition(new MaterialSharedAxis(MaterialSharedAxis.Z, true)
                    .setDuration(1000));

            current.setReenterTransition(new MaterialSharedAxis(MaterialSharedAxis.Z, false)
                    .setDuration(1000));


            NavController navController = Navigation.findNavController(requireActivity(), R.id.navHostFragment);
            navController.navigate(R.id.action_uploadFileFragment_to_fileListFragment);

        }
        return super.onOptionsItemSelected(item);
    }
}
