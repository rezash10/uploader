package com.example.uploader.fragments;

import static com.example.uploader.Utils.GLOBAL_TAG;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewGroupCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.uploader.R;
import com.example.uploader.databinding.FragmentFileListBinding;
import com.google.android.material.transition.MaterialSharedAxis;


public class FileListFragment extends Fragment {




    boolean flag = true;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fragment current = requireActivity().getSupportFragmentManager().findFragmentById(R.id.navHostFragment)
                .getChildFragmentManager().getFragments().get(0);

        current.setEnterTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,true)
        .setDuration(1000));

        current.setReturnTransition(new MaterialSharedAxis(MaterialSharedAxis.Z,false)
        .setDuration(1000));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentFileListBinding binding = FragmentFileListBinding.inflate(inflater);


        FileListFragmentViewModel viewModel = new ViewModelProvider(this).get(FileListFragmentViewModel.class);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());

        if(Build.VERSION.SDK_INT<Build.VERSION_CODES.LOLLIPOP) {
            ViewGroupCompat.setTransitionGroup(binding.filesLayout, true);
        }

        binding.rvFiles.setAdapter(viewModel.adapter);


        binding.rvFiles.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                if(dy>0){

                    int visibleItems = binding.rvFiles.getLayoutManager().getChildCount();
                    int totalItems = binding.rvFiles.getLayoutManager().getItemCount();
                    LinearLayoutManager mLayoutManager = (LinearLayoutManager) binding.rvFiles.getLayoutManager();
                    int pastVisibleItemPos = mLayoutManager.findFirstVisibleItemPosition();



                    if(visibleItems+pastVisibleItemPos==totalItems&&flag){

                        viewModel.loading.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
                            @Override
                            public void onChanged(Boolean aBoolean) {
                                flag = aBoolean;


                            }
                        });


                        viewModel.getFilesFromRestApi(totalItems);


                    }


                }
            }
        });

        viewModel.firstLoad.observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean){
                    binding.gifLoading.setVisibility(View.GONE);
                    binding.rvFiles.setVisibility(View.VISIBLE);
                }
            }
        });



        return binding.getRoot();
    }
}