package com.example.uploader.fragments;


import static com.example.uploader.Utils.GLOBAL_TAG;
import static com.example.uploader.Utils.getFileSize;
import static com.example.uploader.Utils.properByteArray;

import android.app.Application;
import android.content.ContentUris;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.uploader.ProgressRequestBody;
import com.example.uploader.UpdateProgress;
import com.example.uploader.UploadService;
import com.example.uploader.adapters.UploadingFileAdapter;
import com.example.uploader.models.UploadFileMode;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadFileViewModel extends AndroidViewModel {

    //------------Variables--------------//
    String TAG = "Global_Tag";
    Application application;
    public UploadingFileAdapter adapter;
    ExecutorService executorService;
    public MutableLiveData<Boolean> hasUploads = new MutableLiveData<>(false);


    public UploadFileViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        adapter = new UploadingFileAdapter();
        executorService = Executors.newFixedThreadPool(4);

    }


    //------------upload files to server------------//

    public void uploadFile(Uri fileUri, String fileName) {

        String path = getPath(fileUri);

        hasUploads.setValue(true);


        if (path != null) {

            if (!path.equals("-1")) {

                File file = new File(path);

                String extension = MimeTypeMap.getFileExtensionFromUrl(path);


                if (fileName.isEmpty()) {
                    fileName = "File" + UUID.randomUUID();
                }
                fileName += "." + extension;

                String finalName = fileName;
                String fileVolume = getFileSize(file.length());


                //-------------get time from server-------------//



                final UploadFileMode[] model = {null};


                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    Response<String> response = UploadService.apis.getTime().execute();
                                    if (response.isSuccessful()) {

                                        String fileAdded = response.body();


                                        UploadFileMode modelTemp = new UploadFileMode(finalName, fileVolume, fileAdded);

                                        model[0] = modelTemp;
                                        adapter.addNewUpload(modelTemp);

                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(application, "خطایی رخ داده است", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }
                        });

                        thread.start();

                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }



                        UploadFileMode finalModel = model[0];

                        RequestBody requestBody = new ProgressRequestBody(finalName, "multipart/form-data", file, new UpdateProgress() {

                            @Override
                            public void setProgress(int progress, String fileName) {

                                if (finalModel != null) {
                                    adapter.updateUploads(progress, finalModel.getFileName());
                                }
                            }
                        });




                        MultipartBody.Part body = MultipartBody.Part.createFormData("data", finalName, requestBody);
                        MultipartBody.Part fileSize = MultipartBody.Part.createFormData("fileSize", fileVolume);



                        UploadService.apis.uploadMedia(body, fileSize).enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                if (response.isSuccessful()) {
                                    adapter.updateUploads(100, finalName);
                                } else {
                                    adapter.updateUploads(-1, finalName);

                                }
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {

                                adapter.updateUploads(-1, finalName);

                            }
                        });





            }

            else {

                String mimeType = application.getContentResolver().getType(fileUri);
                String extension = mimeType.split("/")[1];

                if (fileName.isEmpty()) {
                    fileName = "file" + UUID.randomUUID();
                }
                fileName += "." + extension;
                sendDocument(fileUri, fileName);
            }


        }

    }

    private String getPath(Uri fileUri) {

        String selection = null;
        String[] selectionArgs = null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(application, fileUri)) {

            if (isExternalStorageDocument(fileUri)) {

                String id = DocumentsContract.getDocumentId(fileUri);
                final String[] split = id.split(":");

                if ("primary".equalsIgnoreCase(split[0])) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }


            } else if (isDownloadsDocument(fileUri)) {


                final String id = DocumentsContract.getDocumentId(fileUri);
                fileUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));


            } else if (isMediaDocument(fileUri)) {


                String id = DocumentsContract.getDocumentId(fileUri);
                final String[] split = id.split(":");
                final String type = split[0];

                if ("image".equals(type)) {
                    fileUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    fileUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    fileUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                } else {
                    return "-1";

                }

                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };


            }
        }


        if ("content".equalsIgnoreCase(fileUri.getScheme())) {

            String[] projection = {
                    MediaStore.MediaColumns.DATA
            };
            Cursor cursor = null;
            try {

                cursor = application.getContentResolver()
                        .query(fileUri, projection, selection, selectionArgs, null);


                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);

                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(fileUri.getScheme())) {
            return fileUri.getPath();
        }
        return null;
    }


    String base = null;
    InputStream inputStream;

    public void sendDocument(Uri fileUri, String fileName) {


        long fileSize = 0;
        try {

            inputStream = application.getContentResolver().openInputStream(fileUri);
            AssetFileDescriptor fileDescriptor = application.getContentResolver().openAssetFileDescriptor(fileUri, "r");
            fileSize = fileDescriptor.getLength();


        } catch (IOException e) {
            e.printStackTrace();
        }


        long finalSize = fileSize;
        String fileVolume = getFileSize(fileSize);


        //-------------get time from server-------------//

        executorService.execute(new Runnable() {
            @Override
            public void run() {

                try {

                    UploadFileMode model = null;

                    Response<String> response = UploadService.apis.getTime().execute();
                    if (response.isSuccessful()) {

                        String fileAdded = response.body();


                         model = new UploadFileMode(fileName, fileVolume, fileAdded);

                        adapter.addNewUpload(model);
                    }


                    byte[] bytesArr = properByteArray(finalSize);

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

                    int read;
                    float updated = 0;

                    while (true) {

                        read = inputStream.read(bytesArr);

                        if (read == -1) {
                            Log.i(GLOBAL_TAG, "finished");
                            adapter.updateUploads(100, model.getFileName());
                            break;
                        }
                        outputStream.write(bytesArr, 0, read);

                        updated += read;
                        int progress = (int) ((updated / finalSize) * 100);



                        adapter.updateUploads(progress, model.getFileName());


                    }

                    if (inputStream != null) {
                        inputStream.close();
                    }

                    byte[] pdfByteArr = outputStream.toByteArray();
                    base = Base64.encodeToString(pdfByteArr, Base64.DEFAULT);

                    //---------upload documents to server-------------//


                    UploadFileMode finalModel = model;
                    UploadService.apis.uploadDoc(fileName, base, fileVolume).enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {

                            if(response.isSuccessful()){
                                Log.i(GLOBAL_TAG, response.body());
                                Log.i(GLOBAL_TAG, String.valueOf(response.code()));
                                adapter.updateUploads(100, finalModel.getFileName());
                            }else{
                                adapter.updateUploads(-1, finalModel.getFileName());

                            }


                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            t.printStackTrace();
                            adapter.updateUploads(-1, finalModel.getFileName());

                        }

                    });


                } catch (IOException e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(application, "خطایی رخ داده است", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });






    }


    public boolean isExternalStorageDocument(Uri uri) {

        return "com.android.externalstorage.documents".equals(uri.getAuthority());

    }

    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


}
