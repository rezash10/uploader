package com.example.uploader.models;

public class UploadFileMode {
    final private String fileName;
    final private String fileSize;
    final private String fileAddedTime;
    private int percent;

    public UploadFileMode(String fileName, String fileSize, String fileAddedTime) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.fileAddedTime = fileAddedTime;

    }


    public String getFileName() {
        return fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getFileAddedTime() {
        return fileAddedTime;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}
