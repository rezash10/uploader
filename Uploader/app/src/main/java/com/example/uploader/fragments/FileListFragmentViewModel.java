package com.example.uploader.fragments;

import static com.example.uploader.Utils.GLOBAL_TAG;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.uploader.models.FileInfoModel;
import com.example.uploader.UploadService;
import com.example.uploader.adapters.FilesAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileListFragmentViewModel extends AndroidViewModel {

    //------------variables-------------//
    Application application;

    public MutableLiveData<Boolean> loading = new MutableLiveData<>();
    public MutableLiveData<Boolean> firstLoad = new MutableLiveData<>(false);

    FilesAdapter adapter;
    public FileListFragmentViewModel(@NonNull Application application) {
        super(application);

        this.application = application;
        adapter = new FilesAdapter();

        getFilesFromRestApi(0);
    }

    public void getFilesFromRestApi(int offset){

        loading.setValue(false);


        if(UploadService.ip==null||UploadService.ip.isEmpty()){
            Toast.makeText(application, "لطفا ip را وارد کنید", Toast.LENGTH_SHORT).show();
            firstLoad.setValue(true);
            return;
        }

        UploadService.getInstance(UploadService.ip);

        if((offset-1)%10==0){
            offset-=1;


        }



        UploadService.apis.getFiles(offset,10).enqueue(new Callback<List<FileInfoModel>>() {
            @Override
            public void onResponse(Call<List<FileInfoModel>> call, Response<List<FileInfoModel>> response) {
                


                if(!response.body().isEmpty()){



                    if(!adapter.filesInfoList.isEmpty()){
                        adapter.filesInfoList.remove(adapter.filesInfoList.size()-1);
                        adapter.notifyItemRemoved(adapter.filesInfoList.size()-1);
                    }

                    adapter.updateList(response.body());



                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            loading.setValue(true);
                            firstLoad.setValue(true);
                        }
                    });
                }else{

                    if(!adapter.filesInfoList.isEmpty()){

                        if(adapter.filesInfoList.size()%10==0){
                            adapter.filesInfoList.remove(adapter.filesInfoList.size()-1);
                            adapter.notifyItemRemoved(adapter.filesInfoList.size()-1);
                        }




                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {


                                loading.setValue(false);


                            }
                        });
                    }else{

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(application, "هیچ فایلی وجود ندارد", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }


                }



            }

            @Override
            public void onFailure(Call<List<FileInfoModel>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }



}
