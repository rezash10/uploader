package com.example.uploader;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import com.example.uploader.databinding.ActivityMainBinding;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivityTAG";
    private NavController navController;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //-----------check permission-------------//
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M) {
            checkPermissions();
        }

        setSupportActionBar(binding.toolbarMain);

        navController = Navigation.findNavController(this,R.id.navHostFragment);
        NavigationUI.setupActionBarWithNavController(this,navController);
    }

    public void checkPermissions(){

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!=
                PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}
            ,1);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length >= 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {

                MaterialAlertDialogBuilder alertBuilder = new MaterialAlertDialogBuilder(this, R.style.MaterialCustomAlertDialog)
                        .setTitle("مهم")
                        .setMessage("دسترسی به حافظه برای این برنامه ضروری میباشد")
                        .setNegativeButton("عدم دسترسی", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).setPositiveButton("فعال سازی", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                goToAppSetting();

                            }
                        }).setCancelable(false);

                alertDialog = alertBuilder.show();





            }
        }
    }

    private void goToAppSetting() {
        Intent settingIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        settingIntent.setData(Uri.fromParts("package", getPackageName(), null));
        startActivityForResult(settingIntent, 1);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {

            checkPermissions();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        return navController.navigateUp()||super.onSupportNavigateUp();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(alertDialog!=null){
            alertDialog.dismiss();
        }
    }
}