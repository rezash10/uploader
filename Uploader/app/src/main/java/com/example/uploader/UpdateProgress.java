package com.example.uploader;

public interface UpdateProgress {
    void setProgress(int progress,String fileName);
}
