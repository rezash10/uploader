package com.example.reminder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reminder.databinding.ModelReminderItemBinding;
import com.example.reminder.model.ReminderEntity;

public class RemindersAdapter extends ListAdapter<ReminderEntity, RemindersAdapter.ViewHolder> {

    public RemindersAdapter() {
        super(new ReminderDiffUtil());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ModelReminderItemBinding binding = ModelReminderItemBinding.inflate(inflater
                ,parent,false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.setupItems(getItem(position));
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ModelReminderItemBinding binding;
        public ViewHolder(@NonNull ModelReminderItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setupItems(ReminderEntity entity){
            binding.setReminderEntity(entity);
            binding.executePendingBindings();

        }
    }
}
