package com.example.reminder.model;

import androidx.room.ColumnInfo;
import androidx.room.Dao;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ReminderDB")
public class ReminderEntity {
    @PrimaryKey(autoGenerate = true)
    private int id = 0;
    @ColumnInfo(name = "year")
    private int year;
    @ColumnInfo(name = "month")
    private int month;
    @ColumnInfo(name = "day")
    private int day;
    @ColumnInfo(name="hour")
    private int hour;
    @ColumnInfo(name = "minute")
    private int minute;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name="body")
    private String body = "شما یک یادآور دارید";
    @ColumnInfo(name = "key")
    private long key;
    @ColumnInfo(name="isEnable")
    private boolean status;


    public ReminderEntity( int year, int month, int day, int hour, int minute, String title, String body
            ,long key,boolean status) {

        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.title = title;
        this.key = key;
        this.status = status;

        if(!body.isEmpty()){
            this.body = body;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getKey() {
        return key;
    }

    public boolean isStatus() {
        return status;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getDate(){
        return year+"/"+month+"/"+day;
    }

    public String getTime(){
        String hourRes = String.valueOf(hour);
        String minuteRes = String.valueOf(minute);
        if(hour<10){
            hourRes = "0".concat(hourRes);
        }
        if (minute<10){
            minuteRes = "0".concat(minuteRes);
        }
        return hourRes+":"+minuteRes;
    }
}
