package com.example.reminder;

import android.graphics.Color;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reminder.model.ReminderEntity;

import java.util.ArrayList;
import java.util.List;


public class Utils {

    public static String G_TAG = "GLOBAL_TAG";
    public static String CHANNEL_ID = "REMINDER_ID";



    public static List<Integer> prepareYear(int year){
        //--------preparing years list----------//

        List<Integer> yearList = new ArrayList<>();
        int yearTemp = year;


        while (year!=yearTemp+10){
            yearList.add(year);
            year++;
        }

        return yearList;
    }

    public static List<Integer> prepareMonth(int month){

        //--------preparing month list----------//

        List<Integer> monthList = new ArrayList<>();

        while (month!=13){
            monthList.add(month);
            month++;
        }

        return monthList;

    }

    public static List<Integer> prepareDay(int day,int month){

        //--------preparing day list----------//

        List<Integer> dayList = new ArrayList<>();


        if(month>6){
            while (day!=31){
                dayList.add(day);
                day++;
            }
        }else{
            while (day!=32){
                dayList.add(day);
                day++;
            }
        }

        return dayList;

    }

    public static List<Integer> prepareHour(int hour){

        //--------preparing hour list----------//

        List<Integer> hourList = new ArrayList<>();

        while (hour!=24){
            if(hour<10){
                hour = Integer.parseInt("0"+hour);
            }
            hourList.add(hour);
            hour++;
        }

        return hourList;

    }

    public static List<Integer> prepareMinute(int minute){

        //--------preparing minute list----------//


        List<Integer> minuteList = new ArrayList<>();


        if(minute==60){
            minute = 0;
        }

        while (minute!=60){
            minuteList.add(minute);
            minute++;
        }

        return minuteList;

    }


    @BindingAdapter("setReminders")
    public static void setReminders(RecyclerView view, List<ReminderEntity> reminders){

        if(reminders!=null&&reminders.size()>0){

            RemindersAdapter adapter = new RemindersAdapter();
            view.setAdapter(adapter);
            adapter.submitList(reminders);

        }

    }

    @BindingAdapter("setDate")
    public static void setDateFun(TextView view,String date ){
        view.setText(date);
    }


    @BindingAdapter("setTime")
    public static void setTime(TextView view,String time){
        view.setText(time);
    }

    @BindingAdapter("reminderStatus")
    public static void setProperClock(ImageView view, boolean status){
        if(status){
            view.setBackgroundResource(R.drawable.onclock);
        }else{
            view.setBackgroundResource(R.drawable.offclock);
        }
    }

    @BindingAdapter("isEnabledFun")
    public static void setHour(TextView view,boolean status){
        if(status){
            view.setTextColor(R.color.purple);
            view.setText("فعال");
        }else{
            view.setTextColor(R.color.gray);
            view.setText("غیرفعال");
        }

    }
}
