package com.example.reminder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.example.reminder.model.ReminderEntity;

public class ReminderDiffUtil extends DiffUtil.ItemCallback<ReminderEntity> {

    @Override
    public boolean areItemsTheSame(@NonNull ReminderEntity oldItem, @NonNull ReminderEntity newItem) {
        return oldItem.getKey()== newItem.getKey();
    }

    @Override
    public boolean areContentsTheSame(@NonNull ReminderEntity oldItem, @NonNull ReminderEntity newItem) {
        return oldItem==newItem;
    }

}
