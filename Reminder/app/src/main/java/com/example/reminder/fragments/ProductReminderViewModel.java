package com.example.reminder.fragments;

import static com.example.reminder.Utils.G_TAG;
import static com.example.reminder.Utils.prepareDay;
import static com.example.reminder.Utils.prepareHour;
import static com.example.reminder.Utils.prepareMinute;
import static com.example.reminder.Utils.prepareMonth;
import static com.example.reminder.Utils.prepareYear;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.reminder.R;
import com.example.reminder.databinding.FragmentProductReminderBinding;
import com.example.reminder.model.ReminderEntity;
import com.example.reminder.model.ReminderReceiver;
import com.example.reminder.model.ReminderRoomDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ProductReminderViewModel extends AndroidViewModel {

    //-----------variables-----------//
    Application application;
    public MutableLiveData<List<Integer>> yearLiveData = new MutableLiveData<>();
    public MutableLiveData<List<Integer>> monthLiveData = new MutableLiveData<>();
    public MutableLiveData<List<Integer>> dayLiveData = new MutableLiveData<>();
    public MutableLiveData<List<Integer>> hourLiveData = new MutableLiveData<>();
    public MutableLiveData<List<Integer>> minuteLiveData = new MutableLiveData<>();

    List<Integer> yearList;
    List<Integer> monthList;
    List<Integer> dayList;
    List<Integer> hourList;
    List<Integer> minuteList;

    String title = "";
    String body = "";



    public ProductReminderViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        settingTimes();
    }



    public void settingTimes(){

        Shamsi shamsi = new Shamsi();
        int year = shamsi.getShamsiYear();
        int month = shamsi.getShamsiMonth();
        int day = shamsi.getShamsiDay();
        int hour = shamsi.getShamsiHour();
        int minute = shamsi.getShamsiMinute();


         yearList = prepareYear(year);
        yearLiveData.setValue(yearList);

         monthList = prepareMonth(month);
        monthLiveData.setValue(monthList);

         dayList = prepareDay(day,month);
        dayLiveData.setValue(dayList);


         hourList = prepareHour(hour);
        hourLiveData.setValue(hourList);


         minuteList = prepareMinute(minute+1);
        minuteLiveData.setValue(minuteList);


    }




    public void setReminderAlertManager(int year,int month,int day,int hour,int minute,String title
            ,String body,long key){

        if(body.isEmpty()){
            body = "Reminder";
        }



        Intent reminderIntent = new Intent(application, ReminderReceiver.class);
        reminderIntent.putExtra("title",title);
        reminderIntent.putExtra("body",body);
        reminderIntent.putExtra("key",key);

        PendingIntent reminderPending = PendingIntent.getBroadcast(application,(int)key,reminderIntent
                ,PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,year);
        calendar.set(Calendar.MONTH,month-1);
        calendar.set(Calendar.DAY_OF_MONTH,day);
        calendar.set(Calendar.HOUR_OF_DAY,hour);
        calendar.set(Calendar.MINUTE,minute);

        Date date =calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm");
        String res = format.format(date);

        AlarmManager alarmManager = (AlarmManager) application.getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),reminderPending);
        }else{
            alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),reminderPending);

        }



//        alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(),reminderPending);
    }






}
