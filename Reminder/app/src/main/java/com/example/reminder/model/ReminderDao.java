package com.example.reminder.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ReminderDao {
    @Insert
    void insertReminder(ReminderEntity entity);

    @Query("SELECT * FROM ReminderDB ORDER BY `id` DESC")
    List<ReminderEntity> getReminders();

    @Query("Update ReminderDB SET `isEnable`=:status WHERE `key` LIKE :key")
    void updateReminder(long key,boolean status);


}
